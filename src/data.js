export default {
	products: [{
			"productName": "Isla Bra",
			"productCode": "LN332",
			"price": 29
		},
		{
			"productName": "Nordic Rose Bra",
			"productCode": "LN336",
			"price": 30
		},
		{
			"productName": "Zentangle Bra",
			"productCode": "FY240",
			"price": 34
		},
		{
			"productName": "Clara Bra",
			"productCode": "PN112",
			"price": 32
		},
		{
			"productName": "Deco Delight Bra",
			"productCode": "FY158",
			"price": 34
		},
		{
			"productName": "Sienna Bra",
			"productCode": "LN328",
			"price": 32
		}
	]
}