// import fetch from "isomorphic-fetch";
import data from "./data.js";

export function fetchProducts( ) {
    return new Promise( resolve => resolve( data ) );
    // return fetch( "http://apiurl.test/products" )
    //     .then( res => res.json( ) )
    //     .then( res => res.products );
}
