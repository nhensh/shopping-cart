import Home from "./components/Home";
import About from "./components/About";
import Contact from "./components/Contact";
import ShoppingBag from "./components/ShoppingBag";

export default [
    {
        path: "/",
        component: Home,
        exact: true,
    },
    {
        path: "/about",
        component: About,
        exact: true,
    },
    {
        path: "/contact",
        component: Contact,
        exact: true,
    },
    {
        path: "/bag",
        component: ShoppingBag,
        exact: true,
    },
];
