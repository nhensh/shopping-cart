import React from "react";
import { connect } from "react-redux";
import { removeFromBag } from "../store";

class Bag extends React.Component {
    constructor() {
        super();
        this.state = {
            title: "Bravissimo",
        };
    }

    render() {
        const cartList = this.props.bag.map(( item, index) =>{
        return (
            <div key={index}>
                <p>{item.productName} - {item.price} $ </p>
                <button className="button"
                        onClick={ () => this.props.removeFromBag( item )} >
                    Remove
                </button>
            </div>
        );
      });

      return (
          <div>
            <h1>Cart</h1>
            <div  className="cart">
              {cartList}
            </div>
          </div>
      );
    }
}

function mapStateToProps( state, props ) {
    return {
        bag: state.bag,
    };
}

function mapDispatchToProps( dispatch ) {
    return {
        removeFromBag: item => dispatch( removeFromBag( item ) )
    };
}

export default connect( mapStateToProps, mapDispatchToProps )( Bag );