import React from "react";
import { Switch, Route } from "react-router-dom";
import { css } from "react-emotion";
import Header from "./Header";
import routes from "../routes";

const grid = css`
    display: grid;
    grid-gap: 10px;
    overflow: hidden;
    grid-template-columns: 5% 90% 5%;
    grid-template-areas:
        "header  header  header"
        "content content content"
        "footer  footer  footer";
`;

const main = css`
    grid-area: content;
`;

class Layout extends React.Component {
    constructor() {
        super();
        this.state = {
            title: "Bravissimo",
        };
    }

    render() {
        return (
            <div className={ grid }>
                <Header title={ this.state.title } />
                <div className={ main }>
                    <Switch>
                        { routes.map( route => <Route key={ route.path } { ...route } /> ) }
                    </Switch>
                </div>
            </div>
        );
    }
}

export default Layout;
