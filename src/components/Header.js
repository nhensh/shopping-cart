import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { css } from "react-emotion";

const header = css`
    grid-area: header;
    background-color: #f4436d;
    color: #fff;
    padding: 30px;
`;

const Header = ( props ) => (
    <div className={ header } >
        <h1>{ props.title }</h1>
        <Link to="/">Home</Link>
        <Link to="/about">About</Link>
        <Link to="/contact">Contact</Link>
        { props.loggedIn && <Link to="/bag">Bag</Link> }
    </div>
);

const mapStateToProps = ( state ) => ( {
    loggedIn: state.loggedIn,
    cart: state.cart,
} );

export default connect( mapStateToProps )( Header );
