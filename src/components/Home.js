import React from "react";
import { connect } from "react-redux";
import styled, { css } from "react-emotion";

import { fetchData, addToBagStore } from "../store";

const productList = css`
    display: flex;
    flex-wrap: wrap;
`;

const productCard = css`
    padding: 2%;
    flex: 1 16%;
    display: flex;
`;

const productInfo = css`
    margin-top: auto;
`;

const AddButton = styled( "button" )`
    text-transform: uppercase;
    letter-spacing: .05em;
    line-height: inherit;
    font-size: 1rem;
    padding: .5rem .75rem;
    display: inline-block;
    border: 0;
    border-radius: .25rem;
    font-weight: bolder;
    text-align: center;
    background-color: #42b9b9;
    color: #fff;
    cursor: pointer;
`;

class Home extends React.Component {
    constructor( props ) {
        super( props );
        this.addToBag = this.addToBag.bind( this );
    }

    componentDidMount( ) {
        if ( this.props.data.length <= 0 ) {
            this.props.fetchData( );
        }
    }

    addToBag( productCode ) {
        this.props.dispatch( addToBagStore( productCode ) );
    }

    render( ) {
        const { data } = this.props;

        return (
            <div>
                <h2>Products</h2>
                <ul className={ productList }>
                    { data.products.map( ( { productCode, productName, price } ) => (
                        <li className={ productCard } key={ productCode } >
                            <div className={ productInfo }>
                                <span>{ productName } - { price }</span>
                                <AddButton onClick={ () => this.addToBag( productCode ) }>
                                    Add to bag
                                </AddButton>
                            </div>
                        </li>
                    ) ) }
                </ul>
            </div>
        );
    }
}
Home.serverFetch = fetchData; // static declaration of data requirements

const mapStateToProps = ( state ) => ( {
    data: state.data,
} );

const mapDispatchToProps = {
    fetchData,
};

export default connect( mapStateToProps, mapDispatchToProps )( Home );
