import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { fetchProducts } from "./api";
import initialState from "./initialState";

export const initializeSession = ( ) => ( {
    type: "INITIALIZE_SESSION",
} );

// ACTIONS
const storeData = ( data ) => ( {
    type: "STORE_DATA",
    data,
} );

const addToBag = ( item ) => ( {
    type: "ADD",
    item,
} );

const removeFromBag = ( item ) => ( {
    type: "REMOVE",
    item,
} );

export const fetchData = ( ) => ( dispatch ) =>
    fetchProducts( ).then( res => dispatch( storeData( res ) ) );

const sessionReducer = ( state = false, action ) => {
    switch ( action.type ) {
        case "INITIALIZE_SESSION":
            return true;
        default: return state;
    }
};

const dataReducer = ( state = [ ], action ) => {
    switch ( action.type ) {
        case "STORE_DATA":
            return action.data;
        default: return state;
    }
};

const bagReducer = (state = initialState.cart, action) => {
    switch ( action.type ) {
        case "ADD":
            return [...state, action.item];
        case "REMOVE":
            return state.filter( i => i.id !== action.item.id );
        default:
            return state;
    }
};


const reducer = combineReducers( {
    bag: bagReducer,
    loggedIn: sessionReducer,
    data: dataReducer,
} );

export default ( initialState ) =>
    createStore( reducer, initialState, applyMiddleware( thunkMiddleware ) );
